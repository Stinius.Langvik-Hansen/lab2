package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    ArrayList<FridgeItem> fridgelist = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridgelist.size();
    }
    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (totalSize() > nItemsInFridge()) {
            fridgelist.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgelist.contains(item)) {
        fridgelist.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgelist.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredlist = new ArrayList<>();
        for (FridgeItem item : fridgelist) {
            if (item.hasExpired()) {
                expiredlist.add(item);
            }
        }
        fridgelist.removeAll(expiredlist);
        return expiredlist;
    }
    
}

